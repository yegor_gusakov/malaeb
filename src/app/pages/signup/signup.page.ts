import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-sign-up',
    templateUrl: './signup.page.html',
    styleUrls: ['./signup.page.scss'],
})
export class SignUpPage implements OnInit {
    isConfirmPasswordVisible: boolean;
    isInputFocused: boolean;
    isPasswordVisible: boolean;
    signUpForm: FormGroup;

    constructor(private formBuilder: FormBuilder) {
        this.init();
    }

    buildForm(): void {
        this.signUpForm = this.formBuilder.group({
            name: new FormControl('', [Validators.required]),
            email: new FormControl('', [
                Validators.required,
                Validators.pattern('^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)|' +
                    '(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$')
            ]),
            password: new FormControl('', [Validators.required]),
            confirmPassword: new FormControl('', [Validators.required])
        }, {
            validator: this.checkPasswords
        });
    }

    checkPasswords(group: FormGroup): null | object {
        const pass = group.controls.password.value;
        const confirmPass = group.controls.confirmPassword.value;
        return pass === confirmPass ? null : { notSame: true };
    }

    init(): void {
        this.isPasswordVisible = false;
        this.isInputFocused = false;
        this.isConfirmPasswordVisible = false;
    }

    onSignUpFormSubmit(): void {
        console.log('Submitted:', this.signUpForm.value);
    }

    removeSpaces(input: object): void {
        input['value'] = input['value'].replace(input['name'] === 'name' ? /\s{2,}/gi : /\s/gi, '');
    }

    ngOnInit() {
        this.buildForm();
    }
}
